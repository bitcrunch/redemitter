/**
 * Created by dzambuto on 14/09/15.
 */
var debug = require('debug')('micro:events');
var Redis = require('ioredis');
var EventEmitter = require('events').EventEmitter;
var config = require('config');

var default_port = 6379;
var default_host = '127.0.0.1';

var instance;

if (config.has('main.redis')) {
  var redis = config.get('main.redis');
  default_host = redis.host;
  default_port = redis.port;
}

module.exports = function (options) {
  function EventBroker (options) {
    var self = this;


    options = options || {};

    this.serializer = options.serializer || JSON;

    this.eventEmitter = new EventEmitter();

    this.clients = {};
    this.clients.pub = new Redis(
      options.port || default_port,
      options.host || default_host
    );
    this.clients.sub = new Redis(
      options.port || default_port,
      options.host || default_host
    );

    this.clients.sub.on('message', function (channel, message) {
      debug('message on channel "%s": %s', channel, JSON.stringify(message));

      try {
        var payload = self.serializer.parse(message);
      }
      catch (err) {
        return self.eventEmitter.emit('error', err);
      }

      self.eventEmitter.emit(channel, payload);
    });
  }

  EventBroker.prototype.on = function (channel, fn) {
    debug('subscribe on channel "%s"', channel);
    this.clients.sub.subscribe(channel, function (err) {
      if (err) return this.eventEmitter.emit('error', err);
      this.eventEmitter.on(channel, fn);
    });
  };

  EventBroker.prototype.emit = function (channel, message) {
    debug('emit message on channel "%s": %s', channel, JSON.stringify(message));
    try {
      var payload = this.serializer.stringify(message);
    }
    catch (err) {
      return this.eventEmitter.emit('error', err);
    }

    return this.clients.pub.publish(channel, payload);
  };

  if (!instance)
    instance = new EventBroker(options);

  return instance;
};