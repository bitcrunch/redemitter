/**
 * Created by dzambuto on 05/10/15.
 */
var util = require('util')
  , EventEmitter = require('events').EventEmitter;

function Bus (options) {
  options = options || {};

  EventEmitter.call(this);
}

util.inherits(Bus, EventEmitter);

function implementError (callback) {
  var err = new Error('Please implement this function!');
  if (callback) callback(err);
  throw err;
}

Bus.prototype.connect = implementError;
Bus.prototype.disconnect = implementError;
Bus.prototype.publish = implementError;
Bus.prototype.subscribe = implementError;